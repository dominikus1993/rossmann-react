import { SearchItem, SearchResult, Result, ApiProduct } from "../utils/servertypes";
import { SearchProductUrl } from "../utils/serverUrls";

export interface Product {
    readonly name: string;
    readonly id: number;
    readonly productImgSrc: string;
    readonly price: number;
    readonly content: string;
}

export interface ProductSearchBoxState {
    readonly products: Product[];
    readonly productId: number;
}

interface DownloadProductsAction { type: "DOWNLOAD_PRODUCT"; payload: number; }
interface FailedDownloadProductAction { type: "DOWNLOAD_PRODUCT_FAILED"; err?: any; }
interface ReceiveDownloadProductAction { type: "DOWNLOAD_PRODUCT_RECEIVED"; payload: Result<ApiProduct>; }

type ProductSearchBoxActions = DownloadProductsAction | FailedDownloadProductAction | ReceiveDownloadProductAction;

export const actionCreators = {
    search: (phrase: string) => {
        return async (dispatch: any) => {
            try {
                dispatch({ type: "DOWNLOAD_PRODUCT", payload: phrase });
                const response = await fetch(SearchProductUrl(phrase, 735));
                const result: Result<ApiProduct> = await response.json();
                dispatch({ type: "DOWNLOAD_PRODUCT_RECEIVED", payload: result });
            } catch (error) {
                dispatch({ type: "DOWNLOAD_PRODUCT_FAILED", err: error });
            }
        }
    }
}

export const reducer = (state: ProductSearchBoxState = { products: [], phrase: "" }, action: ProductSearchBoxActions): ProductSearchBoxState => {
    if (action.type === "SEARCH_PRODUCT_RECEIVED") {
        if (action.payload.IsSuccess) {
            const products = action.payload.Value.Items.map(p => ({ name: p.Name, id: p.Id, productImgSrc: p.SmallPictureUrl, price: p.Price, content: p.Content }));
            return { ...state, products };
        }
        return state;
    } else if (action.type === "SEARCH_PRODUCTS") {
        return { ...state, phrase: action.payload };
    }
    return state;
}