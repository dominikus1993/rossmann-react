import * as ProductSearchBox from "./ProductSearchBox";
import { bindActionCreators } from 'redux'
import { combineReducers } from "redux";
import { applyMiddleware } from "redux";
import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import logger from "redux-logger";
import thunk from "redux-thunk";

export interface ApplicationState {
  serachBox: ProductSearchBox.ProductSearchBoxState;
}

export const reducers = {
  serachBox: ProductSearchBox.reducer
}

export type AppActions = typeof ProductSearchBox.actionCreators;

export type ActionCreators = AppActions;

export const mapDispatchToProps = (dispatch: any) => {
  return {
    search: bindActionCreators(ProductSearchBox.actionCreators.search, dispatch),
  }
}

const initState: ApplicationState = {
  serachBox: {
      products: [],
      phrase: ""
  }
}

const combinedReducers = combineReducers<ApplicationState>(reducers);
const middleware = applyMiddleware(logger, thunk);

export const initStore = (initialState: ApplicationState = initState, options: any) => {
  return createStore<ApplicationState>(combinedReducers, initialState, composeWithDevTools(middleware));
};