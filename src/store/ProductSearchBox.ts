import { SearchItem, SearchResult, Result } from "../utils/servertypes";
import { SearchProductUrl } from "../utils/serverUrls";

export interface Product {
    readonly name: string;
    readonly id: number;
    readonly productImgSrc: string;
    readonly price: number;
    readonly content: string;
}

export interface ProductSearchBoxState {
    readonly products: Product[];
    readonly phrase: string;
}

interface SearchProductsAction { type: "SEARCH_PRODUCTS"; payload: string; }
interface FailedSearchProductAction { type: "SEARCH_PRODUCTS_FAILED"; err?: any; }
interface ReceiveSearchProductAction { type: "SEARCH_PRODUCT_RECEIVED"; payload: Result<SearchResult>; }

type ProductSearchBoxActions = SearchProductsAction | FailedSearchProductAction | ReceiveSearchProductAction;

export const actionCreators = {
    search: (phrase: string) => {
        return async (dispatch: any) => {
            try {
                dispatch({ type: "SEARCH_PRODUCTS", payload: phrase });
                const response = await fetch(SearchProductUrl(phrase, 735));
                const result: Result<SearchResult> = await response.json();
                dispatch({ type: "SEARCH_PRODUCT_RECEIVED", payload: result });
            } catch (error) {
                dispatch({ type: "SEARCH_PRODUCTS_FAILED", err: error });
            }
        }
    }
}

export const reducer = (state: ProductSearchBoxState = { products: [], phrase: "" }, action: ProductSearchBoxActions): ProductSearchBoxState => {
    if (action.type === "SEARCH_PRODUCT_RECEIVED") {
        if (action.payload.IsSuccess) {
            const products = action.payload.Value.Items.map(p => ({ name: p.Name, id: p.Id, productImgSrc: p.SmallPictureUrl, price: p.Price, content: p.Content }));
            return { ...state, products };
        }
        return state;
    } else if (action.type === "SEARCH_PRODUCTS") {
        return { ...state, phrase: action.payload };
    }
    return state;
}