import { Component } from "react";
import { Product } from "../../store/ProductSearchBox";
import Link from "next/link";

interface ProductListProps {
    readonly products: Product[];
}

export const ProductBox = ({ product }: { product: Product }) => {
    const href = {
        pathname: '/product',
        query: { productId: product.id }
      }
    const as = {
        pathname: `/product/${product.id}`,
      }
      
    return <div className="tile is-6 is-parent">
        <article className="tile is-child notification is-info">
            <Link href={href} as={as}><p className="title">{product.name}</p></Link>
            <p className="subtitle" dangerouslySetInnerHTML={{__html: product.content}}></p>
            <figure className="image is-16by9">
                <img src={product.productImgSrc} />
            </figure>
        </article>
    </div>;
}


export class ProductList extends Component<ProductListProps, {}> {
    public render() {
        return <div className="tile is-ancestor is-vertical">
                {this.props.products.map(p => (<ProductBox product={p} key={p.id} />))}
        </div>;
    }
};
