import { Component } from "react";
import {SearchBox} from "./SearchBox";
import { ProductList } from "./ProductList";
import * as ProductSearchBoxStore from "../../store/ProductSearchBox";
import { ApplicationState } from "../../store";
import { connect } from "react-redux";

type ProductSearchBoxProps = ProductSearchBoxStore.ProductSearchBoxState & typeof ProductSearchBoxStore.actionCreators;
    
 export class ProductSearchBox extends Component<ProductSearchBoxProps , {}> {
    public render() {
        return <div>
            <SearchBox search={this.props.search} phrase={this.props.phrase} ></SearchBox>
            <ProductList products={this.props.products} ></ProductList>
        </div>;
    }
}