
import { Component } from "react";


export const SearchBox = ({ phrase, search }: { phrase: string, search: (phrase: string) => void }) => {
  return (<div className="field">
    <label className="label">Szukaj</label>
    <div className="field has-addons has-addons-centered" >
      <div className="control is-expanded">
        <input className="input" type="text" placeholder="Wpisz szukaną fraze" value={phrase} onChange={(e) => { search(e.target.value) }} />
      </div>
      <div className="control">
        <a className="button is-info">
          Szukaj
      </a>
      </div>
    </div>
  </div>);
}

