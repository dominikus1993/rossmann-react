export interface Result<A>{
    readonly Value: A;
    readonly IsSuccess: boolean;
}

export interface SearchItem {
    readonly Id: number;
    readonly SmallPictureUrl: string;
    readonly Name: string;
    readonly Price: number;
    readonly Content: string;
}

export interface SearchResult {
    readonly TotalCount: number;
    readonly Items: SearchItem[];
}


export interface ApiProduct {
    Name: string;
    Price: string;
    LargePictureUrl: string;
}