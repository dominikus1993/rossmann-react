export const BaseUrl = "http://api.rossmann.pl/";
export const SearchProductUrl = (phrase: string, shopNumber: number = 735) => `${BaseUrl}/search?Query=${phrase}&ShopNumber=${shopNumber}`;
export const GetProductById = (productId: number, shopNumber: number = 735) => `${BaseUrl}/shops/${shopNumber}/products/${productId}`;