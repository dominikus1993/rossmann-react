
import { Component } from "react";
import { ApplicationState, reducers, mapDispatchToProps, initStore } from "../store";
import { compose, combineReducers, applyMiddleware, createStore } from "redux";
import logger from "redux-logger";
import { Provider } from "react-redux";
import * as ApplicationStore from "../store/index";
import withRedux from "next-redux-wrapper";
import { ProductSearchBox } from "../components/ProductSearchBox/ProductSearchBox";
import { createDevTools } from 'redux-devtools';
import thunk from "redux-thunk";
import DevTools from "../components/DevTools";
import { composeWithDevTools } from 'redux-devtools-extension';
import * as css from "../styles/style.scss";
import Head from "next/head";


export type AppProps = ApplicationStore.ApplicationState & ApplicationStore.ActionCreators;

export class App extends Component<AppProps, {}> {
    public render() {
        return <div>
            <Head>
                <title>This page has a title 🤔</title>
                <meta charSet='utf-8' />
                <meta name="description" content="That's it!" />
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <style dangerouslySetInnerHTML={{__html: css }}></style>
            </Head>
            <div className="container" >
                <ProductSearchBox {...this.props.serachBox} search={this.props.search} ></ProductSearchBox>
            </div>
        </div>;
    }
}

export default withRedux(initStore, (state) => state, mapDispatchToProps)(App);
